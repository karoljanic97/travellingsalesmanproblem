/**
    * TSP - Travelling Travelling Salesman Problem
    * Karol Janic
    * April 2021
**/


#include <iostream>
#include <fstream>
#include <vector>
#include <limits>
#include <stdlib.h>
#include <time.h>
#include <math.h>

using namespace std;

fstream dane;
int N;
int **miasta;
bool *odwiedzone;

int metoda_najblizszego_sasiada(int punkt_startowy, vector<int> *trasa)
{
    odwiedzone = new bool[N];
    for(int i = 0; i<N; i++)
        odwiedzone[i] = false;

    (*trasa).clear();
    (*trasa).push_back(punkt_startowy);
    odwiedzone[punkt_startowy] = true;

    int obecny = punkt_startowy;
    int calkowita_odleglosc = 0;
    int minimalna;
    int minimalny;

    while((*trasa).size() != N)
    {
        minimalna = numeric_limits<int>::max();
        for(int i = 0; i<N; i++)
        {
            if(miasta[obecny][i] < minimalna && !odwiedzone[i])
            {
                minimalny = i;
                minimalna = miasta[obecny][i];
            }
        }
        (*trasa).push_back(minimalny);
        obecny = minimalny;
        calkowita_odleglosc += minimalna;
        odwiedzone[minimalny] = true;
    }
    calkowita_odleglosc += miasta[obecny][punkt_startowy];
    (*trasa).push_back(punkt_startowy);


    return calkowita_odleglosc;
}

/// metoda najblizszego sasiada ze stalego wierzcholka 0
int metoda_0()
{
    vector<int> najlepsza_trasa;
    int minimalna_dlugosc = metoda_najblizszego_sasiada(0, &najlepsza_trasa);

/** wypisanie wyniku
    for(vector<int>::iterator it = najlepsza_trasa.begin(); it != najlepsza_trasa.end(); it++)
        cout << *it << "  " ;
    cout << endl;
**/

    return minimalna_dlugosc;
}

/// metoda najblizszego sasiada uruchomiona z kazdego wierzcholka, wybierane najlepsze rozwiazanie
int metoda_1()
{
    vector<int> trasa;
    vector<int> najlepsza_trasa;

    int minimalna_dlugosc = metoda_najblizszego_sasiada(0, &trasa);
    int mini;
    for(int i = 1; i<N; i++)
    {
        mini = metoda_najblizszego_sasiada(i, &trasa);
        najlepsza_trasa.clear();
        for(vector<int>::iterator it = trasa.begin(); it != trasa.end(); it++)
            najlepsza_trasa.push_back(*it);
        if(mini < minimalna_dlugosc)
        {
            minimalna_dlugosc = mini;
            najlepsza_trasa.clear();
            for(vector<int>::iterator it = trasa.begin(); it != trasa.end(); it++)
                najlepsza_trasa.push_back(*it);
        }
    }

/** wypisanie wyniku
    for(vector<int>::iterator it = najlepsza_trasa.begin(); it != najlepsza_trasa.end(); it++)
        cout << *it << "  " ;
    cout << endl;
**/

    return minimalna_dlugosc;
}

/// random walk
int metoda_2(int liczba_powtorzen)
{
    srand(time(NULL));

    int *trasa = new int[N];
    int *najlepsza_trasa = new int[N];
    int minimalna_dlugosc, nowa_dlugosc;

    /// ustwienie naturalne
    vector<int> trasa_0;
    metoda_najblizszego_sasiada(0, &trasa_0);
    int h = 0;
    for(vector<int>::iterator it = trasa_0.begin(); it != trasa_0.end(); it++)
    {
        trasa[h] = (*it);
        najlepsza_trasa[h] = (*it);
        h++;
    }

    minimalna_dlugosc = 0;
    for(int i = 0; i<N-1; i++)
    {
        minimalna_dlugosc += miasta[trasa[i]][trasa[i+1]];
    }
    minimalna_dlugosc += miasta[trasa[0]][trasa[N-1]];

    /// losowanie numeru tablicy od 0 do N-1, zamiana elementu wylosowanego i elementu po jego prawej stronie
    int pozycja_zmiany;
    for(int i = 0; i<liczba_powtorzen; i++)
    {
        pozycja_zmiany = rand()%(N-1);
        swap(trasa[pozycja_zmiany], trasa[pozycja_zmiany+1]);
        nowa_dlugosc = 0;
        for(int j = 0; j<N-1; j++)
        {
            nowa_dlugosc += miasta[trasa[j]][trasa[j+1]];
        }
        nowa_dlugosc += miasta[trasa[0]][trasa[N-1]];

        if(nowa_dlugosc < minimalna_dlugosc)
        {
            minimalna_dlugosc = nowa_dlugosc;
            for(int k = 0; k<N; k++)
                najlepsza_trasa[k] = trasa[k];
        }
        else
        {
            swap(trasa[pozycja_zmiany], trasa[pozycja_zmiany+1]);
        }
    }

/** wypisanie wyniku
    for(int i = 0; i<N; i++)
        cout << najlepsza_trasa[i] << "  ";
    cout << endl;
*/

    delete [] trasa;
    delete [] najlepsza_trasa;

    return minimalna_dlugosc;
}

/// metoda symulowanego wyzarzania
int metoda_3(float temperatura_poczatkowa, float temperaura_minimalna, float maksymalna_liczba_powtorzen, float chlodzenie)
{
    srand(time(NULL));

    int *trasa = new int[N];
    int *najlepsza_trasa = new int[N];
    int minimalna_dlugosc, nowa_dlugosc;

    /// ustwienie naturalne
    vector<int> trasa_0;
    metoda_najblizszego_sasiada(0, &trasa_0);
    int h = 0;
    for(vector<int>::iterator it = trasa_0.begin(); it != trasa_0.end(); it++)
    {
        trasa[h] = (*it);
        najlepsza_trasa[h] = (*it);
        h++;
    }

    minimalna_dlugosc = 0;
    for(int i = 0; i<N-1; i++)
    {
        minimalna_dlugosc += miasta[trasa[i]][trasa[i+1]];
    }
    minimalna_dlugosc += miasta[trasa[0]][trasa[N-1]];

    /// wyzarzanie
    int iteracja, pozycja_zmiany;
    float temperatura = temperatura_poczatkowa;
    float p,r;

    while(temperatura > temperaura_minimalna)
    {
        iteracja = 0;
        while(iteracja < maksymalna_liczba_powtorzen)
        {
            pozycja_zmiany = rand()%(N-1);
            swap(trasa[pozycja_zmiany], trasa[pozycja_zmiany+1]);
            nowa_dlugosc = 0;
            for(int j = 0; j<N-1; j++)
            {
                nowa_dlugosc += miasta[trasa[j]][trasa[j+1]];
            }
            nowa_dlugosc += miasta[trasa[0]][trasa[N-1]];

            p = exp((double)(-(abs(minimalna_dlugosc-nowa_dlugosc))/temperatura));
            r = (float)((rand()%1000) / 1000.0);

            if(nowa_dlugosc < minimalna_dlugosc || r < p)
            {
                if(nowa_dlugosc < minimalna_dlugosc)
                {
                    for(int k = 0; k<N; k++)
                        najlepsza_trasa[k] = trasa[k];
                }
                minimalna_dlugosc = nowa_dlugosc;
            }
            else
            {
                swap(trasa[pozycja_zmiany], trasa[pozycja_zmiany+1]);
            }

            iteracja++;
        }

        temperatura *= chlodzenie;
    }

/** wypisanie wyniku
    for(int i = 0; i<N; i++)
        cout << najlepsza_trasa[i] << "  ";
    cout << endl;
*/
    nowa_dlugosc = 0;
    for(int j = 0; j<N-1; j++)
    {
        nowa_dlugosc += miasta[trasa[j]][trasa[j+1]];
    }
    nowa_dlugosc += miasta[trasa[0]][trasa[N-1]];


    delete [] trasa;
    delete [] najlepsza_trasa;

    return nowa_dlugosc;
}

/// samodostrajajacy sie algorytm wyzarzania
int metoda_4(float temperatura_0_min, float temperatura_0_max, float dt, float wspolczynnik_chlodzenia_min, float wspolczynnik_chlodzenia_max, float dc)
{
    int najlepsze_rozwiazanie = metoda_3(temperatura_0_min, 0.2, 1000, wspolczynnik_chlodzenia_min);
    int obecne_rozwiazanie;
    float najlepsza_temp, najlepsze_c;

    for(float t = temperatura_0_min; t < temperatura_0_max; t+=dt)
    {
        for(float c = wspolczynnik_chlodzenia_min; c < wspolczynnik_chlodzenia_max; c+=dc)
        {
            for(int s = 0; s<10; s++)
            {
                obecne_rozwiazanie = metoda_3(t,0.2,1000,c);
                if(obecne_rozwiazanie < najlepsze_rozwiazanie)
                {
                    najlepsze_rozwiazanie = obecne_rozwiazanie;
                    najlepsza_temp = t;
                    najlepsze_c = c;
                }
            }
        }
    }
    cout << "Dobrana temperatura poczatkowa: " <<najlepsza_temp << "  Dobrany wspolczynnik chlodzenia: " << najlepsze_c << endl << endl;

    return najlepsze_rozwiazanie;
}


int main()
{

/// wczytanie danych, inicjalizacja zmiennych i macierzy sasiedztwa
    dane.open("42_miast.txt", ios::in); /// albo "5_miast.txt" albo "42_miast.txt"
    N = 42;

    miasta = new int*[N];
    for(int i = 0; i<N; i++)
        miasta[i] = new int[N];

    for(int i = 0; i<N; i++)
    {
        for(int j = 0; j<N; j++)
        {
            dane >> miasta[i][j];
        }
    }

/// rozwiazania problemu
    cout << "Metoda najblizszego wierzcholka z wierzcholka 0: " << metoda_0() << endl << endl;
    cout << "Najlepszy wynik z metody najblizszego wierzcholka wywolanej dla kazedgo wierzcholka: " << metoda_1() << endl << endl;
    cout << "Metoda random walk z 10 powtorzeniami: " << metoda_2(10) << endl << endl;
    cout << "Metoda random walk z 1000 powtorzeniami: " << metoda_2(1000) << endl << endl;
    cout << "Metoda random walk z 100000 powtorzeniami: " << metoda_2(100000) << endl << endl;
    int najlepszy = metoda_3(10, 0.2, 1000, 0.95);
    int obecny;
    for(int i = 0; i<100; i++)
    {
        obecny = metoda_3(10, 0.2, 1000, 0.95);
        if(obecny<najlepszy)
            najlepszy = obecny;
    }
    cout << "Najlepszy wynik z 1000 wywolan metody symulowane wyrzazania: " << najlepszy << endl << endl;
    cout << "Najlepszy wynik metody samodostrajajacego sie algorytmu wyzazania: " << metoda_4(20.0, 40.0, 1.0, 0.9, 0.99, 0.01) << endl;


/// zwolnienie pamieci i zamkniecie pliku
    for(int i = 0; i<N; i++)
    {
        delete [] miasta[i];
    }
    delete [] miasta;
    delete [] odwiedzone;

    dane.close();

    return 0;
}
